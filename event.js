import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const eventModel = new Schema({
    title: { type: String   },
    author: { type: String }
})
export default mongoose.model('events', eventModel)
