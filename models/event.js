import mongoose from 'mongoose';

var eventSchema = {
    name: String,
    date: String,
    time: String,
    phone: String,
    img: String,
    slots: String
  };
  

export default mongoose.model('events', eventSchema)
