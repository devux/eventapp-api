const express = require("express")
const mongoose = require("mongoose")
const app = express();
var cors=require('cors');
app.use(cors({
  origin: 'http://localhost:3000',
  credentials: true
}));

app.use(function (req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
res.setHeader('Access-Control-Allow-Credentials', true);
next();
});


const MLAB_USERNAME = "devakumar";
const MLAB_PASSWORD = "password123"
const mongoURI = `mongodb://${MLAB_USERNAME}:${MLAB_PASSWORD}@dbh36.mlab.com:27367/eventbooking`


mongoose.connect(
mongoURI,
{
    useNewUrlParser: true,
    useUnifiedTopology: true
}
);

var eventSchema = {
    name: String,
    date: String,
    time: String,
    phone: String,
    img: String,
    slots: String
  };
  
  var Event = mongoose.model('events', eventSchema);

  app.get('/eventlist', (req, res) => {
      var inputQuery = req.query
      Event.find(inputQuery, function(err, eventlist) {
          res.send(eventlist)
      });
  
  })
  

const port = 5000;

app.listen(port, () => {

console.log(`app started on port ${port}`);

});

